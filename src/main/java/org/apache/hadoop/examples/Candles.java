package org.apache.hadoop.examples;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.*;
import org.apache.hadoop.util.GenericOptionsParser;

import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Candles {

    public static final String OUTPUT_NAME = "ToolBasedOutput";
    public static final int START_OFFSET_MS = 0; //start at 00:00:00:000

    public static class CandlesMapper
        extends Mapper<Object, Text, MyKey, MyData>{

        private String dateFrom;
        private String dateTo;
        private int candleWidthMs;
        private Pattern pattern;

        private int alignedStartMs;
        private int alignedEndMs;

        private MyKey myKey = new MyKey();
        private MyData myData = new MyData();

        public void setup(Mapper.Context context)
                throws IOException, InterruptedException {
            try {
                //get configuraton
                Configuration conf = context.getConfiguration();
                candleWidthMs = Integer.parseInt(conf.get("candle.width"));
                if(candleWidthMs <= 0) {
                    throw new Exception();
                }
                //YYYYMMDD
                dateFrom = conf.get("candle.date.from");
                dateTo = conf.get("candle.date.to");
                //HHMM
                String timeFrom = conf.get("candle.time.from");
                String timeTo = conf.get("candle.time.to");
                String toolPattern = conf.get("candle.securities");

                int hours = Integer.parseInt(timeFrom.substring(0, 2));
                int minutes = Integer.parseInt(timeFrom.substring(2, 4));
                int timeFromMs = (hours * 60 + minutes) * 60 * 1000; //convert to milliseconds

                hours = Integer.parseInt(timeTo.substring(0, 2));
                minutes = Integer.parseInt(timeTo.substring(2, 4));
                int timeToMs = (hours * 60 + minutes) * 60 * 1000; //convert to milliseconds


                int timeFromMod = (timeFromMs - START_OFFSET_MS) % candleWidthMs;
                int timeToMod = (timeToMs - START_OFFSET_MS) % candleWidthMs;

                if(timeFromMod == 0) {
                    alignedStartMs = timeFromMs;
                } else {
                    alignedStartMs = timeFromMs + timeFromMod;
                }

                if(timeToMod == 0) {
                    alignedEndMs = timeToMs;
                } else {
                    alignedEndMs = timeToMs + timeToMod;
                }

                pattern = Pattern.compile(toolPattern);

            } catch(Exception e) {
                throw new InterruptedException("Wrong configuration");
            }
        }

        public void map(Object key, Text value, Context context
                        ) throws IOException, InterruptedException {

            String toolName = "", moment = "", dateStr = "";
            String timeStr = "", priceStr = "";
            int hours, minutes, seconds, milliseconds;
            float price;
            try {
                //read line
                String line = value.toString();
                String[] values = line.split(",");
                toolName = values[0];
                moment = values[2];
                dateStr = moment.substring(0, 8);
                timeStr = moment.substring(8);
                priceStr = values[4];

                //convert to milliseconds
                //HHMMSSmmm
                hours = Integer.parseInt(timeStr.substring(0, 2));
                minutes = Integer.parseInt(timeStr.substring(2, 4));
                seconds = Integer.parseInt(timeStr.substring(4, 6));
                milliseconds = Integer.parseInt(timeStr.substring(6, 9));

                price = Float.parseFloat(priceStr);

            } catch(Exception e) {
                //return
                return;
            }

            Matcher m = pattern.matcher(toolName);
            if(!m.matches()) {
                //tool name no matches regular expression
                return;
            }

            int res1 = dateStr.compareTo(dateFrom);
            int res2 = dateStr.compareTo(dateTo);
            if(res1 < 0 || res2 >= 0) {
                //date not in the interval
                return;
            }

            int timeMs = ((60 * hours + minutes) * 60 + seconds) * 1000 + milliseconds;

            if(timeMs > alignedEndMs || timeMs < alignedStartMs) {
                //time not in the interval
                return;
            }

            //get interval index (use it as key)
            int timeIntervalId = (timeMs - START_OFFSET_MS) / candleWidthMs;

            //set key
            myKey.setDate(dateStr);
            myKey.setInterval(timeIntervalId);
            myKey.setTool(toolName);
            //set data
            myData.setPrice(price);
            myData.setMoment(moment);
            //output
            context.write(myKey, myData);
        }
    }

    public static class CandlesCombiner
            extends Reducer<MyKey,MyData,MyKey,MyData> {
        private MyData myData = new MyData();

        public void reduce(MyKey key, Iterable<FloatWritable> values,
                           Context context
        ) throws IOException, InterruptedException {

            float min = Float.MAX_VALUE;
            String minMoment = "";
            float max = -Float.MAX_VALUE;
            String maxMoment = "";

            float open = 0;
            String openMoment = null;
            float close = 0;
            String closeMoment = null;

            Iterator iterator = values.iterator();
            while(iterator.hasNext()) {
                MyData myData = (MyData)iterator.next();
                float price = myData.getPrice();
                String moment = myData.getMoment();

                if(openMoment == null) {
                    openMoment = moment;
                    open = price;
                } else {
                    int res = openMoment.compareTo(moment);
                    if(res > 0) {
                        openMoment = moment;
                        open = price;
                    }
                }

                if(closeMoment == null) {
                    closeMoment = moment;
                    close = price;
                } else {
                    int res = closeMoment.compareTo(moment);
                    if(res < 0) {
                        closeMoment = moment;
                        close = price;
                    }
                }

                if(max < price) {
                    max = price;
                    maxMoment = moment;
                }
                if(min > price) {
                    min = price;
                    minMoment = moment;
                }
            }

            myData.setMoment(openMoment);
            myData.setPrice(open);
            context.write(key, myData);

            myData.setMoment(minMoment);
            myData.setPrice(min);
            context.write(key, myData);

            myData.setMoment(maxMoment);
            myData.setPrice(max);
            context.write(key, myData);

            myData.setMoment(closeMoment);
            myData.setPrice(close);
            context.write(key, myData);
        }
    }

    public static class CandlesReducer
        extends Reducer<MyKey,MyData,Text,Text> {
        private Text resultData = new Text();
        private Text resultKey = new Text();

        private int candleWidthMs;

        private final int MS_IN_HOUR = 1000 * 60 * 60;
        private final int MS_IN_MINUTE = 1000 * 60;
        private final int MS_IN_SEC = 1000;

        private MultipleOutputs multipleOutputs;

        @Override
        public void setup(Context context)
                throws IOException, InterruptedException {
            try {
                //get configuraton
                Configuration conf = context.getConfiguration();
                candleWidthMs = Integer.parseInt(conf.get("candle.width"));
            } catch(Exception e) {
                throw new InterruptedException("Wrong configuration");
            }

            multipleOutputs = new MultipleOutputs(context);
        }

        @Override
        public void cleanup(Context context)
                throws IOException, InterruptedException {
            multipleOutputs.close();
        }

        public void reduce(MyKey key, Iterable<MyData> values,
                           Context context
        ) throws IOException, InterruptedException {
            float min = Float.MAX_VALUE;
            float max = -Float.MAX_VALUE;

            float open = 0;
            String openMoment = null;
            float close = 0;
            String closeMoment = null;

            Iterator iterator = values.iterator();
            while(iterator.hasNext()) {
                MyData myData = (MyData)iterator.next();
                float price = myData.getPrice();
                String moment = myData.getMoment();

                if(openMoment == null) {
                    openMoment = moment;
                    open = price;
                } else {
                    int res = openMoment.compareTo(moment);
                    if(res > 0) {
                        openMoment = moment;
                        open = price;
                    }
                }

                if(closeMoment == null) {
                    closeMoment = moment;
                    close = price;
                } else {
                    int res = closeMoment.compareTo(moment);
                    if(res < 0) {
                        closeMoment = moment;
                        close = price;
                    }
                }

                if(max < price) {
                    max = price;
                }
                if(min > price) {
                    min = price;
                }
            }

            String date = key.getDate();
            int intervalId = key.getInterval();
            String tool = key.getTool();

            int momentMs = intervalId * candleWidthMs;

            int hours = momentMs / MS_IN_HOUR;
            int momentModHours = momentMs % MS_IN_HOUR;

            int minutes = momentModHours / MS_IN_MINUTE;
            int momentModMinutes = momentModHours % MS_IN_MINUTE;

            int seconds = momentModMinutes / MS_IN_SEC;
            int momentModSec = momentModMinutes % MS_IN_SEC;

            int milliseconds = momentModSec;

            //HHMMSSmmm
            String moment = date + String.format("%02d%02d%02d%03d", hours, minutes, seconds, milliseconds);

            String resData = moment + ","  + Float.toString(open) + "," + Float.toString(max)
                    + "," + Float.toString(min) + "," + Float.toString(close);
            resultData.set(resData);

            String resKey = key.getTool();
            String outFilename = resKey;
            resultKey.set(resKey);

            multipleOutputs.write(OUTPUT_NAME, resultKey, resultData, outFilename);
            //context.write(resultKey, resultData);
        }
    }

    public static void printUsage() {
        final String usage = "Usage: candle <candle.width> <candle.securities>"
                + "<candle.date.from> <candle.date.to>"
                + "<candle.time.from> <candle.time.to> <input-file> <out-dir>";
        System.err.println(usage);
    }

    public static void main(String[] args) throws Exception {

        long startTime = System.currentTimeMillis();

        Configuration conf = new Configuration();
        //conf.set("mapred.textoutputformat.separator", ",");
        conf.set("mapreduce.output.textoutputformat.separator", ",");
        //conf.setBoolean("mapreduce.multipleoutputs.counters", false);

        String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
        if (otherArgs.length != 2) {
            printUsage();
            System.exit(2);
        }

        //arguments
        String inputPath = "", outputDir = "";
        //try to read arguments
        try {
            inputPath = otherArgs[0];
            outputDir = otherArgs[1];
        } catch(Exception e) {
            printUsage();
            System.exit(2);
        }

        String jobName = "Candle";

        Job job = new Job(conf, jobName);
        job.setJarByClass(Candles.class);
        job.setMapperClass(CandlesMapper.class);
        job.setCombinerClass(CandlesCombiner.class);
        job.setReducerClass(CandlesReducer.class);

        job.setMapOutputKeyClass(MyKey.class);
        job.setMapOutputValueClass(MyData.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        job.setInputFormatClass(TextInputFormat.class);
        LazyOutputFormat.setOutputFormatClass(job, TextOutputFormat.class);

        MultipleOutputs.addNamedOutput(job, OUTPUT_NAME, CSVTextOutputFormat.class, Text.class, Text.class);

        FileInputFormat.addInputPath(job, new Path(inputPath));
        FileOutputFormat.setOutputPath(job, new Path(outputDir));

        int res = job.waitForCompletion(true) ? 0 : 1;

        long elapsedTime = System.currentTimeMillis() - startTime;
        System.err.println("time elapsed: " + Long.toString(elapsedTime) + "(ms)");
        System.exit(res);
    }
}