package org.apache.hadoop.examples;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class MyData implements Writable{
    String moment;
    float price;

    public void write(DataOutput out) throws IOException {
        out.writeUTF(moment);
        out.writeFloat(price);
    }

    public void readFields(DataInput in) throws IOException {
        moment = in.readUTF();
        price = in.readFloat();
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setMoment(String moment) {
        this.moment = moment;
    }

    public float getPrice() {
        return this.price;
    }

    public String getMoment() {
        return this.moment;
    }
}
