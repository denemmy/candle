package org.apache.hadoop.examples;

import org.apache.hadoop.io.WritableComparable;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class MyKey implements WritableComparable<MyKey> {
    public String tool;
    public String date;
    public int timeIntervalId;

    public MyKey() {
        this.tool = "";
        this.date = "";
        this.timeIntervalId = 0;
    }

    public void write(DataOutput out) throws IOException {
        out.writeUTF(date);
        out.writeInt(timeIntervalId);
        out.writeUTF(tool);
    }

    public void readFields(DataInput in) throws IOException {
        date = in.readUTF();
        timeIntervalId = in.readInt();
        tool = in.readUTF();
    }

    @Override
    public String toString() {
        return date + " " + timeIntervalId + " " + tool;
    }

    @Override
    public int compareTo(MyKey other) {
        int res1 = this.tool.compareTo(other.tool);
        if(res1 == 0) {
            int res2 = this.date.compareTo(other.date);
            if(res2 == 0) {
                if(this.timeIntervalId < other.timeIntervalId) {
                    return -1;
                } else if(this.timeIntervalId > other.timeIntervalId) {
                    return 1;
                } else {
                    //date and time is equal
                    return 0;
                }
            } else {
                return res2;
            }
        } else {
            return res1;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof MyKey) {
            MyKey otherKey = (MyKey)obj;
            int res1 = this.date.compareTo(otherKey.date);
            int res2 = this.timeIntervalId == otherKey.timeIntervalId ? 0 : 1;
            int res3 = this.tool.compareTo(otherKey.tool);
            if(res1 == 0 && res2 == 0 && res3 == 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        String concatStr = this.tool + this.date + Integer.toString(this.timeIntervalId);
        return concatStr.hashCode();
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTool(String tool) {
        this.tool = tool;
    }

    public void setInterval(int index) { this.timeIntervalId = index; }

    public String getTool() {
        return this.tool;
    }

    public String getDate() {
        return this.date;
    }

    public int getInterval() { return this.timeIntervalId; }
}